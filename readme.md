Pong example project using bevy 0.8

***

Demonstrates:
- Creating a 2D bevy project
- Using rapier for 2D physics (kinematic and dynamic rigid bodies) with customisation of physics behaviour (adjusting ball velocity depending on which part of paddle it hits)
- Logical separation into modules and using as bevy plugins
- Using app stages, and calling systems at specific app stages
- Retrieving sprites from a spritesheet (unsure if I am doing this correctly, as I break the same spritesheet down twice to get different sized sprites)
- Loading assets onto the asset server using a GameTextures struct
- User input, and separating user input from movement logic (so code for movement also re-used for AI paddle)
- Solved game for AI behaviour by predicting ball position, impossible for AI to lose even with speed handicap (doing this in bevy was easier than with godot due to ECS)
- Using rapier collision events to adjust ball movement

***

Potential improvements:
- Clean up code, and separate systems where possible into multiple systems
- Calculate the ball position for AI once, only after it hits the player paddle and the y movement becomes positive
- Add easier AI difficulties by adjusting the predicted ball position with an error margin
- Scoring system
- UI
- Main menu and game scenes
- Pause and in-game menu