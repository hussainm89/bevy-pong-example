use bevy::{prelude::*, sprite::Rect};

use crate::{AppState, cleanup_system, paddle::PaddlePlugin, ball::BallPlugin};

const SPRITESHEET: &str = "breakout_spritesheet.png"; // 128 x 384

pub struct GamePlugin;

#[derive(Component)]
pub struct Moveable {
    pub velocity: Vec3,
    pub speed: f32,
}

pub struct GameTextures {

    pub breakout_spritesheet: Handle<TextureAtlas>,
    pub red_ball_index: usize,
    pub red_paddle_index: usize,
    pub green_paddle_index: usize,
}

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum GameState {
    Init,
    PaddlesSpawned,
    BallSpawned,
    BallDied,
}

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app
        .add_system_set(SystemSet::on_enter(AppState::Init).with_system(load_assets))
        .add_plugin(PaddlePlugin)
        .add_plugin(BallPlugin)
        .add_system_set(SystemSet::on_enter(AppState::Game).with_system(setup_game_camera))
        .add_system_set(SystemSet::on_exit(AppState::Game).with_system(cleanup_system));
    }
}

fn load_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {

    let texture_handle: Handle<Image> = asset_server.load(SPRITESHEET);

    let mut breakout_spritesheet = TextureAtlas::new_empty(texture_handle.clone(), Vec2::new(128., 384.));

    let ball_index = breakout_spritesheet.add_texture(
        bevy::sprite::Rect {min: Vec2::new(64.0, 0.), max: Vec2::new(96., 32.)});
    let red_paddle_index = breakout_spritesheet.add_texture(
        Rect {min: Vec2::new(0., 256.+32.0), max: Vec2::new(128., 256.+64.)});
    let green_paddle_index = breakout_spritesheet.add_texture(
        Rect {min: Vec2::new(0., 256.+64.0), max: Vec2::new(128., 256.+96.)});
    let breakout_spritesheet = texture_atlases.add(breakout_spritesheet);
    let game_textures = GameTextures { breakout_spritesheet, red_ball_index: ball_index, red_paddle_index, green_paddle_index };
    commands.insert_resource(game_textures);
}

fn setup_game_camera(
    mut commands: Commands,
) {

    commands.spawn_bundle(Camera2dBundle::default());
}
