use bevy::{prelude::{Plugin, App, Commands, Res, Transform, Vec3, Component, Query, With, Vec2, EventReader, Entity, State, ResMut, DespawnRecursiveExt, SystemSet}, sprite::{SpriteSheetBundle, TextureAtlasSprite}, time::Time};
use rand::{thread_rng, Rng};
use bevy_rapier2d::{prelude::*};
use crate::{GameState, AppState, paddle::Controlled, game::{GameTextures, Moveable}};



pub struct BallPlugin;

#[derive(Component)]
pub struct Ball;

const START_BALL_SPEED: f32 = 500.0;

impl Plugin for BallPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_system_set(SystemSet::on_enter(AppState::Game).with_system(ball_spawn_system))
        .add_system_set(SystemSet::on_enter(GameState::BallDied).with_system(ball_spawn_system))
        .add_system_set(
            SystemSet::on_update(AppState::Game)
                .with_system(ball_move_system)
                .with_system(ball_physics_system));
    }
}
fn ball_spawn_system(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    mut game_state: ResMut<State<GameState>>,

) {    
    // set initial velocity
    let mut rng = thread_rng();
    let x:f32 = rng.gen_range(-1.0..=1.0);
    let y:f32 = rng.gen_range(-1.0..=1.0);

    commands.spawn_bundle(SpriteSheetBundle {
        sprite: TextureAtlasSprite::new(game_textures.red_ball_index),
        texture_atlas: game_textures.breakout_spritesheet.clone(),
        ..Default::default()
    })
    .insert(Moveable {velocity: Vec3::new(x, y, 0.), speed: START_BALL_SPEED})
    .insert(Ball)
    .insert(RigidBody::Dynamic)
    .insert(Collider::ball(16.))
    .insert(ActiveEvents::COLLISION_EVENTS)
    .insert(Velocity::zero());
    game_state.overwrite_set(GameState::BallSpawned).unwrap();
}

fn ball_physics_system(
    mut contact_events: EventReader<CollisionEvent>,
    mut ball_query: Query<(&Transform, &mut Moveable), With<Ball>>,
    paddle_query: Query<(Entity, &Transform), With<Controlled>>,
) {

    // collision with paddle (custom physics)            
    for contact_event in contact_events.iter() {

        if let CollisionEvent::Started(_h1, _h2, _h3) = contact_event {
            for (paddle_entity, paddle_transform) in paddle_query.iter() {
                if &paddle_entity != _h1 && &paddle_entity != _h2 {
                    continue;
                }

                for (ball_transform, mut ball_moveable) in ball_query.iter_mut() {

                    ball_moveable.velocity.y *= -1.;
                    // divide by distance between centre and edge (64 pixels for paddle (width / 2) + 16 pixels for ball = 80)
                    ball_moveable.velocity.x = ((ball_transform.translation.x - paddle_transform.translation.x) / 80.).clamp(-1., 1.);

                }
            }
        }
    }

}

// we can make this better by separating the velocity calculation from the physics implementation
fn ball_move_system(
    mut commands: Commands,
    mut ball_query: Query<(Entity, &Transform, &mut Velocity, &mut Moveable), With<Ball>>,
    // paddle_query: Query<(Entity, &GlobalTransform), With<Controlled>>,
    time: Res<Time>,
    // mut contact_events: EventReader<CollisionEvent>,
    mut game_state: ResMut<State<GameState>>,
) {
    // for (paddle_entity, paddle_transform) in paddle_query.iter() {
        for (ball_entity, ball_transform, mut ball_rb_vel, mut ball_moveable) in ball_query.iter_mut() {
            
            // skew y velocity to be vertical
            if ball_moveable.velocity.y < 0.5 && ball_moveable.velocity.y > 0. {
                ball_moveable.velocity.y += 1.0 * time.delta().as_secs_f32();
            } else if ball_moveable.velocity.y > -0.5 && ball_moveable.velocity.y < 0. {
                ball_moveable.velocity.y -= 1.0 * time.delta().as_secs_f32();
            }

            // cusotmise the physics
            ball_rb_vel.linvel = Vec2::new(ball_moveable.velocity.x, ball_moveable.velocity.y).normalize() * ball_moveable.speed;

            // // bounce off sides
            if ball_transform.translation.x < -960./2. && ball_moveable.velocity.x < 0. || ball_transform.translation.x > 960./2. && ball_moveable.velocity.x > 0. {
                ball_moveable.velocity.x *= -1.;
            }
            
            // die if out of y bounds
            if ball_transform.translation.y < -960./2. || ball_transform.translation.y > 960./2. {
                commands.entity(ball_entity).despawn_recursive();
                game_state.overwrite_set(GameState::BallDied).unwrap();
            }

            // accelerate with time
            ball_moveable.speed += 7.*time.delta_seconds();
        }
    // }
}