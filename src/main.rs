// region: -- TODO

// fade effect between scenes

// add scoring system. first to 5 wins -> menu pops up

// add timer before ball starts moving (3..2..1..play)

// add menu at game to resume/restart or go to main menu scene

// add background image to main menu and to game

// add screenshake whenever ball impacts, and particles whenever ball impacts

// add menu and game music

// add button sfx (hover and click), game start sfx (same time as 3..2..1), ball impact sfx, and point +1 sfx


// rebind controls - and save and load locally

// adjust music, sfx, voice volumes - and save and load locally

// graphics - fullscreen toggle, resolution if possible - save and load locally

// online leaderboard

// export to web

// upload to itch and share the source

// endregion: -- TODO
use bevy::prelude::*;
use bevy_inspector_egui::WorldInspectorPlugin;
use game::{GamePlugin, GameState};
use interface::InterfacePlugin;
use main_menu::MainMenuPlugin;
use bevy_rapier2d::prelude::*;

mod paddle;
mod ball;
mod main_menu;
mod interface;
mod game;



#[derive(Clone, PartialEq, Eq, Debug, Hash)]
enum AppState {
    Init,
    MainMenu,
    Game,
    Quit,
}

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(WindowDescriptor {
            width: 960.,
            height: 960.,
            title: "My First Bevy Pong".to_string(),
            resizable: false,
            ..Default::default()
        })
        .add_state(AppState::Init)
        .add_state(GameState::Init)
        .add_plugins(DefaultPlugins)
        .add_plugin(WorldInspectorPlugin::new()) // for debug
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default()) // physics
        .add_plugin(InterfacePlugin) // common interface elements
        .add_plugin(MainMenuPlugin) // main menu scene
        .add_plugin(GamePlugin) // game scene
        .run();
}

fn cleanup_system(
    mut commands: Commands,
    query: Query<Entity>,
) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}
