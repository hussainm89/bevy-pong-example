// Provides UI elements

use bevy::{prelude::*, ui::FocusPolicy};

use crate::{AppState};

pub struct InterfacePlugin;

impl Plugin for InterfacePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(AppState::Init).with_system(load_assets))
        .add_system_set(SystemSet::on_update(AppState::MainMenu).with_system(button_system));
    }
}

pub struct UiAssets {
    pub font: Handle<Font>,
    pub button: Handle<Image>,
    pub button_hover: Handle<Image>,
    pub button_pressed: Handle<Image>,
}

// load assets
fn load_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut app_state: ResMut<State<AppState>>,
) {
    let ui_assets: UiAssets = UiAssets {
        font: asset_server.load("boxy_bold.ttf"),
        button: asset_server.load("button_normal.png"),
        button_hover: asset_server.load("button_hover.png"),
        button_pressed: asset_server.load("button_pressed.png"),
    };

    commands.insert_resource(ui_assets);
    app_state.overwrite_set(AppState::MainMenu).unwrap();
}

pub fn create_default_node_bundle(
    size: Size<Val>,
) -> NodeBundle {
    NodeBundle {
        style: bevy::ui::Style {
            flex_direction: bevy::ui::FlexDirection::ColumnReverse,
            align_self: bevy::ui::AlignSelf::Center,
            align_items: bevy::ui::AlignItems::Center,
            justify_content: bevy::ui::JustifyContent::Center,
            size,
            margin: bevy::ui::UiRect::all(bevy::ui::Val::Auto),
            ..Default::default()
        },
        color: bevy::prelude::Color::NONE.into(),
        ..Default::default()
    }
}

pub fn spawn_basic_button<T: Component>(
    parent: &mut ChildBuilder,
    button_text: &str,
    button_action: T,
    ui_assets: &Res<UiAssets>,
) {
    parent.spawn_bundle(create_default_button_bundle())
    .with_children(|parent| {
        parent.spawn_bundle(create_default_button_image_bundle(ui_assets))
        .insert(FocusPolicy::Pass)
        .with_children(|parent| {
            parent.spawn_bundle(create_default_text_bundle(ui_assets, button_text));
        });
    })
    .insert(button_action);
}

fn create_default_button_bundle (
) -> ButtonBundle {
    ButtonBundle {
        style: Style {
            flex_direction: FlexDirection::ColumnReverse,
            align_self: AlignSelf::Center,
            align_items: AlignItems::Center,
            justify_content: JustifyContent::Center,
            size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
            margin: UiRect::all(Val::Auto),
            ..Default::default()
        },
        color: Color::NONE.into(),
        ..Default::default()
    }
}

fn create_default_button_image_bundle(
    ui_assets: &Res<UiAssets>,
) -> ImageBundle {
    ImageBundle {
        style: Style {
            flex_direction: FlexDirection::ColumnReverse,
            size: Size::new(Val::Percent(100.), Val::Percent(100.)),
            justify_content: JustifyContent::Center,
            align_items: AlignItems::Center,
            ..Default::default()
        },
        image: ui_assets.button.clone().into(),
        ..Default::default()
    }
}

fn create_default_text_bundle(
    ui_assets: &Res<UiAssets>,
    button_text: &str,
) -> TextBundle {
    TextBundle {
        text: Text::from_section(
            button_text,
            TextStyle {
                font: ui_assets.font.clone(),
                font_size: 40.0,
                color: Color::rgb(0.9,0.9,0.9),
            },
        ).with_alignment(TextAlignment::CENTER),
        focus_policy: FocusPolicy::Pass,
        ..Default::default()
    }
}

fn button_system(
    ui_assets: Res<UiAssets>,
    buttons_query: Query<(&Interaction, &Children), (Changed<Interaction>, With<Button>)>,
    mut image_query: Query<&mut UiImage>
) {
    // loop through all the buttons and their interaction status and their children
    for (interaction, children) in buttons_query.iter() {
        // get the 2nd child of the button (presumably this is the image)
        let child = children.iter().next().unwrap();
        // use the ui image query to retrieve the specific image that matches the 2nd child of the button
        // which is the image's button
        let mut image = image_query.get_mut(*child).unwrap();
        match interaction {
            Interaction::Clicked => {
                image.0 = ui_assets.button_pressed.clone();
            }
            Interaction::Hovered => {
                image.0 = ui_assets.button_hover.clone();
            }
            Interaction::None => {
                image.0 = ui_assets.button.clone();
            }
        }
    }
}