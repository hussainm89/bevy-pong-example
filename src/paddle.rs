
use bevy::{prelude::{Plugin, App, Commands, Res, Component, Query, State, ResMut, Transform, Vec3, Input, KeyCode, With, Vec2, Without, SystemSet }, sprite::{SpriteSheetBundle, TextureAtlasSprite}, time::Time};
use bevy_rapier2d::prelude::*;
use crate::{GameState, ball::Ball, AppState, game::{GameTextures, Moveable}};

const START_PADDLE_SPEED: f32 = 500.;

pub struct PaddlePlugin;

#[derive(Component)]
pub struct Controlled {
    pub player: bool,
    speed_multiplier: f32,
}

impl Plugin for PaddlePlugin {
    fn build(&self, app: &mut App) {
        app
        .add_system_set(SystemSet::on_enter(AppState::Game).with_system(paddle_spawn_system))
        .add_system_set(
            SystemSet::on_update(AppState::Game)
                .with_system(paddle_input_system)
                .with_system(paddle_movement_system)
        );
    }
}

fn paddle_spawn_system(
    mut commands: Commands,
    game_textures: Res<GameTextures>,
    mut game_state: ResMut<State<GameState>>,
) {
    
    let mut spawn_paddle = |paddle_sprite_index: usize, pos: Vec3, player: bool, speed_multiplier: f32| {
        commands.spawn_bundle(SpriteSheetBundle {
            sprite: TextureAtlasSprite::new(paddle_sprite_index),
            texture_atlas: game_textures.breakout_spritesheet.clone(),
            transform: Transform {
                translation: pos,
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Moveable {velocity: Vec3::new(0.,0., 0.), speed: START_PADDLE_SPEED})
        .insert(Controlled {player, speed_multiplier})
        .insert(RigidBody::KinematicVelocityBased)
        .insert(Collider::cuboid(64., 16.))
        .insert(Velocity::zero());
    };
    
    spawn_paddle(game_textures.green_paddle_index, Vec3::new(0., -960./2. + 32., 10.), true, 1.);
    spawn_paddle(game_textures.red_paddle_index, Vec3::new(0., 960./2. - 32., 10.), false, 0.5);

    game_state.overwrite_set(GameState::PaddlesSpawned).unwrap();
}

fn paddle_movement_system(
    mut query: Query<(&Transform, &mut Velocity, &Moveable, &Controlled)>,
) {
    for (transform, mut rb_vels, moveable, controlled) in query.iter_mut() {
        rb_vels.linvel.x = moveable.velocity.x * moveable.speed * controlled.speed_multiplier;
        if transform.translation.x < -960./2. + 64. && moveable.velocity.x < 0. || transform.translation.x > 960./2. - 64. && moveable.velocity.x > 0. {
            rb_vels.linvel.x = 0.;
        }
    }
}

fn paddle_input_system(
    kb: Res<Input<KeyCode>>,
    mut paddle_query: Query<(&Transform, &mut Moveable, &Controlled), Without<Ball>>,
    ball_query: Query<(&Transform, &Moveable), With<Ball>>,
    time: Res<Time>,
) {
    for (paddle_transform, mut moveable, controlled) in paddle_query.iter_mut() {
        // PLAYER INPUT
        if controlled.player {
            moveable.velocity.x = if kb.pressed(KeyCode::A) || kb.pressed(KeyCode::Left) {
                -1.0
            } else if kb.pressed(KeyCode::D) || kb.pressed(KeyCode::Right) {
                1.0
            } else {
                0.
            };
        } else {
            // AI INPUT
            for (ball_transform, ball_moveable) in ball_query.iter() {
                // avoid freezing bug at the start
                if time.delta().as_secs_f32() == 0. {
                    continue;
                }
                let predicted_ball_position = get_predicted_ball_position(
                    ball_transform.translation,
                    time.delta().as_secs_f32(),
                    paddle_transform.translation,
                    Vec2::new(ball_moveable.velocity.x, ball_moveable.velocity.y),
                    ball_moveable.speed);
                if (predicted_ball_position.x - paddle_transform.translation.x).abs() < 32. {
                    moveable.velocity.x = 0.;
                    continue;
                }
                moveable.velocity.x = if predicted_ball_position.x < paddle_transform.translation.x {
                    -1.
                } else if predicted_ball_position.x > paddle_transform.translation.x {
                    1.
                } else {
                    0.
                };
            }
        }
    }
}

fn get_predicted_ball_position(
    ball_translation: Vec3,
    delta_time: f32,
    ai_paddle_translation: Vec3,
    ball_movement: Vec2,
    ball_speed: f32,
) -> Vec3 {
    let mut est_pos = ball_translation;
    let mut est_ball_movement = ball_movement;
    if est_ball_movement.y < 0. {
        return Vec3::new(0., 0., 0.);
    }
    while est_pos.y < ai_paddle_translation.y {
        if est_pos.x < -960./2. || est_pos.x > 960./2. {
            est_ball_movement = Vec2::new(-est_ball_movement.x, est_ball_movement.y);
        }
        est_pos.x += est_ball_movement.x * ball_speed * delta_time;
        est_pos.y += est_ball_movement.y * ball_speed * delta_time;
    }
    est_pos
}
