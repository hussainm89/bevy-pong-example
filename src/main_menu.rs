use bevy::{prelude::{Plugin, App, SystemSet, Res, ResMut, EventWriter, State, Commands, Camera2dBundle, BuildChildren, Query, Changed, Component}, app::AppExit, ui::{Interaction, Size, Val}, };

use crate::{AppState, cleanup_system, interface::{UiAssets, create_default_node_bundle, spawn_basic_button}};


pub struct MainMenuPlugin;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_system_set(SystemSet::on_enter(AppState::MainMenu).with_system(setup_menu_system))
        .add_system_set(SystemSet::on_exit(AppState::MainMenu).with_system(cleanup_system))
        .add_system_set(SystemSet::on_update(AppState::MainMenu)
            .with_system(button_press_system));
    }
}
#[derive(Component)]
enum MenuButtonAction {
    Play,
    Exit,
}

fn setup_menu_system(
    mut commands: Commands,
    ui_assets: Res<UiAssets>,
) {
    commands.spawn_bundle(Camera2dBundle::default());

    // Make root node for buttons
    commands.spawn_bundle(create_default_node_bundle(Size::new(Val::Percent(50.0), Val::Percent(20.0))))
    .with_children(|parent| {

        // Play button
        spawn_basic_button(parent, "Play", MenuButtonAction::Play, &ui_assets);

        // empty node (for spacing)
        parent.spawn_bundle(create_default_node_bundle(Size::new(Val::Percent(50.0), Val::Percent(20.0))));

        // Quit button
        spawn_basic_button(parent, "Quit", MenuButtonAction::Exit, &ui_assets);
    });
}

fn button_press_system(
    buttons_query: Query<(&Interaction, &MenuButtonAction), Changed<Interaction>>,
    mut app_state: ResMut<State<AppState>>,
    mut exit: EventWriter<AppExit>,
) {
    for (interaction, button_action) in buttons_query.iter() {
        if interaction == &Interaction::Clicked {
            match button_action {
                MenuButtonAction::Play => {
                    app_state.set(AppState::Game).unwrap();
                },
                MenuButtonAction::Exit => {
                    app_state.set(AppState::Quit).unwrap();
                    // even better, we can make a exit method and point to it when the state changes instead
                    exit.send(AppExit);
                },
            }
        }
    }
}
